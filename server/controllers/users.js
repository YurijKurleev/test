const User = require('../models').User;

module.exports = {
    create(req, res) {
        const body = JSON.parse(req.body.data);
        const newUser = {
            name: body.name,
            surname: body.surname,
            age: body.age,
            email: body.email
        };
        if (req.file) {
            newUser.photoUrl = `http://${req.headers.host}/uploads/${req.file.filename}`;
        }
        return User
            .create(newUser)
            .then(user => res.status(201).send(user))
            .catch(error => {
                res.status(400).send(error);
            });
    },
    list(req, res) {
        return User
            .all()
            .then(users => res.send(users))
            .catch(error => res.status(404).send(error))
    },
    getOne(req, res) {
        return User
            .findById(req.params.id)
            .then(users => res.send(users))
            .catch(error => res.status(404).send(error))
    },
    update(req, res) {
        return User
            .findById(req.params.id)
            .then(user => {
                if (!user) {
                    return res.status(404).send({
                        message: 'User Not Found',
                    });
                }
                const body = JSON.parse(req.body.data);
                const newUser = {
                    name: body.name || user.name,
                    surname: body.surname || user.surname,
                    age: body.age || user.age,
                    email: body.email || user.email
                };
                if (req.file) {
                    newUser.photoUrl = `http://${req.headers.host}/uploads/${req.file.filename}`;
                }
                return user
                    .update(newUser)
                    .then(() => res.status(200).send(user))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },
    remove(req, res) {
        return User
            .findById(req.params.id)
            .then(user => {
                if (!user) {
                    return res.status(404).send({
                        message: 'User Not Found',
                    });
                }
                return user
                    .destroy()
                    .then(() => res.status(204).send(user))
                    .catch(error => res.status(400).send(error));
            })
            .catch(error => res.status(400).send(error));
    }
};