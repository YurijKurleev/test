'use strict';

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        surname: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        age: {
            type: DataTypes.INTEGER,
            allowNull: true,
            defaultValue: 0
        },
        email: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        photoUrl: {
            type: DataTypes.STRING,
            allowNull: true,
        }
    });

    // User.associate = (models) => {
    //     User.hasMany(models.TodoItem, {
    //         foreignKey: 'todoId',
    //         as: 'todoItems',
    //     });
    // };

    return User;
};