const usersController = require('../controllers').users;
const multer  = require('multer');
const upload = multer({ dest: 'uploads/'});

module.exports = (app) => {
    app.get('/api', (req, res) => res.status(200).send({
        message: 'Welcome to the Users API!',
    }));
    app.get('/api/users', usersController.list);
    app.get('/api/users/:id', usersController.getOne);
    app.post('/api/users', upload.single('avatar'), usersController.create);
    app.put('/api/users/:id', upload.single('avatar'), usersController.update);
    app.delete('/api/users/:id', usersController.remove);
};